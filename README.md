# Analizador Lexico

Analizador léxico en C++

Objetivo: Construir un analizador de léxico a partir de la teoría vista en clase para el lenguaje Spes.
Lenguaje para el desarrollo del proyecto C++.

# Especificaciones
- Palabras Reservadas: Las palabras reservadas estarán escritas en minúsculas de acuerdo conla siguiente lista:
  - class, endclass, int, float, char, string, bool, if, else, elseif, endif, do, eval, enddo, while, endwhile, read, write, def,as,for, endfor, private, public, protected, library, func, endfunc, main, endmain, true, false.
- Identificadores: Inician con letra mayúscula o minúscula, seguido por cero o más letras mayúsculas o minúsculas, dígito o guión bajo con el único requisito de que no podrá terminar con guión bajo.
- Constantes numéricas de enteros, reales y de notación científica: Dígitos+[.Dígitos+ [(E|e) [+|-] Dígitos+ ]]
- Constante carácter, contiene un solo carácter entre comilla simple: ‘a’
- Constante string contiene cero o más elementos entre comilla doble: “esta es una cadena”
- Operadores aritméticos: +, -, *, /, %, ++, --, **
- Operadores lógicos: &&, ||, !
- Operadores Relacionales: ==, <, <=, >, >=, !=
- Operadores de asignacion: =, +=, -=, *=, /=, %=
- Signos de puntuación: ;, .., ,
- Signos de agrupación: (, ), [, ]
- Comentarios: De bloque /* con cero o máscaracteres y puede tener 1 o másrenglones */, comentario en línea #con cero o más caracteres hasta encontrar el enter
- Delimitadores: espacio en blanco, enter y tabulador