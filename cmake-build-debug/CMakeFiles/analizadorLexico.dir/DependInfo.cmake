# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/magnimussoftware/analizadorLexico/src/domain/Codes/Errors.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/Codes/Errors.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/Codes/Messages.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/Codes/Messages.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/Grammeme/Grammeme.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/Grammeme/Grammeme.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/Lexeme/Lexeme.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/Lexeme/Lexeme.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/AbstractStateMachine.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/AbstractStateMachine.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/Clasification.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/Clasification.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/ConcreteMachines/Asterisk.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/ConcreteMachines/Asterisk.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/ConcreteMachines/Character.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/ConcreteMachines/Character.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/ConcreteMachines/FrontSlash.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/ConcreteMachines/FrontSlash.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/ConcreteMachines/Identifier.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/ConcreteMachines/Identifier.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/ConcreteMachines/Initial.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/ConcreteMachines/Initial.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/ConcreteMachines/Minus.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/ConcreteMachines/Minus.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/ConcreteMachines/Numeric.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/ConcreteMachines/Numeric.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/ConcreteMachines/Plus.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/ConcreteMachines/Plus.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/ConcreteMachines/String.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/ConcreteMachines/String.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/StatesMachine/StateMachine.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/StatesMachine/StateMachine.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/domain/Token/Token.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/domain/Token/Token.cpp.obj"
  "C:/magnimussoftware/analizadorLexico/src/main.cpp" "C:/magnimussoftware/analizadorLexico/cmake-build-debug/CMakeFiles/analizadorLexico.dir/src/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "MSVC")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
