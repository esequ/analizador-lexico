#ifndef ANALIZADORLEXICO_CLASIFICATION_H
#define ANALIZADORLEXICO_CLASIFICATION_H


#include <string>

class Clasification {
public:
    static int getAscii(char character);
    static bool isLetter(char code);
    static bool isNumber(char code);
    static bool isUnderScore(char code);
    static bool isSingleQuote(char code);
    static bool isDoubleQuote(char code);
    static bool isFrontSlash(char code);
    static bool isAdmiration(char code);
    static bool isEquals(char code);
    static bool isPlus(char code);
    static bool isMinus(char code);
    static bool isAsterisk(char code);
    static bool isPercentaje(char code);
    static bool isLogicSymbol(char code);
    static bool isPuncSymbol(char code);
    static bool isAgrupSymbol(char code);
    static bool isCommentSymbol(char code);
    static bool isDelimiter(char code);
};


#endif //ANALIZADORLEXICO_CLASIFICATION_H
