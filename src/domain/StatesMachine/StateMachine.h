#ifndef ANALIZADORLEXICO_STATEMACHINE_H
#define ANALIZADORLEXICO_STATEMACHINE_H
#include "AbstractStateMachine.h"
#include <vector>
#include "../Lexeme/Lexeme.h"
class StateMachine {
public:
    AbstractStateMachine* actualMachine;
    explicit StateMachine(char character);
    void nextStep(char actualChar);
    int actualCode;
    std::string element;
    std::vector<Lexeme> elements;
    void transition(char character);
    void addElement(char character);

    void changeMachine(char character);
};


#endif //ANALIZADORLEXICO_STATEMACHINE_H
