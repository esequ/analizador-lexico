#ifndef ANALIZADORLEXICO_ABSTRACTSTATEMACHINE_H
#define ANALIZADORLEXICO_ABSTRACTSTATEMACHINE_H

#include <string>

class AbstractStateMachine {
public:
    virtual ~AbstractStateMachine() = default;
    char actualCharacter;
    std::string state;
    virtual int evaluate() = 0;
};


#endif //ANALIZADORLEXICO_ABSTRACTSTATEMACHINE_H
