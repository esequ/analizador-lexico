#ifndef ANALIZADORLEXICO_CHARACTER_H
#define ANALIZADORLEXICO_CHARACTER_H

#include <string>
#include "../AbstractStateMachine.h"
#include "../../Codes/Messages.h"
#include "../../Codes/Errors.h"
class Character: public AbstractStateMachine {
public:
    Character();
    int evaluate() override;
};


#endif //ANALIZADORLEXICO_CHARACTER_H
