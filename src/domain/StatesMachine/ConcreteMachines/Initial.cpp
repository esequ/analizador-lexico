#include <iostream>
#include "Initial.h"
#include "../Clasification.h"
Initial::Initial() {
    this->state = "q0";
}

int Initial::evaluate() {
    if (Clasification::isDelimiter(this->actualCharacter)){
        return 0;
    }
    if (Clasification::isLetter(this->actualCharacter)) {
        return 1;
    }
    if (Clasification::isNumber(this->actualCharacter)) {
        return 3;
    }
    if(Clasification::isSingleQuote(this->actualCharacter)) {
        return 9;
    }
    if(Clasification::isDoubleQuote(this->actualCharacter)){
        return 12;
    }
    if(Clasification::isAgrupSymbol(this->actualCharacter)) {
        return 5;
    }
    if(Clasification::isLogicSymbol(this->actualCharacter)) {
        return 6;
    }
    if(Clasification::isAdmiration(this->actualCharacter)) {
        return 8;
    }
    if(Clasification::isEquals(this->actualCharacter)) {
        return 9;
    }
    if(Clasification::isPlus(this->actualCharacter)) {
        return 14;
    }
    if(Clasification::isMinus(this->actualCharacter)) {
        return 16;
    }
    if(Clasification::isAsterisk(this->actualCharacter)) {
        return 18;
    }
    if(Clasification::isFrontSlash(this->actualCharacter)) {
        return 20;
    }
    if(Clasification::isPercentaje(this->actualCharacter)) {
        return 14;
    }
    if(Clasification::isPuncSymbol(this->actualCharacter)) {
        return 15;
    }
    if(Clasification::isAgrupSymbol(this->actualCharacter)) {
        return 15;
    }
    if(Clasification::isCommentSymbol(this->actualCharacter)) {
        return 16;
    }
    return 500;
}
