#ifndef ANALIZADORLEXICO_MINUS_H
#define ANALIZADORLEXICO_MINUS_H


#include "../AbstractStateMachine.h"

class Minus: public AbstractStateMachine {
public:
    Minus();
    int evaluate() override;
};


#endif