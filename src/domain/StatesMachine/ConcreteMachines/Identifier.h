#ifndef ANALIZADORLEXICO_IDENTIFIER_H
#define ANALIZADORLEXICO_IDENTIFIER_H


#include <string>
#include "../AbstractStateMachine.h"
#include "../../Codes/Messages.h"
#include "../../Codes/Errors.h"
class Identifier: public AbstractStateMachine{
public:
    Identifier();
    int evaluate() override;
};


#endif //ANALIZADORLEXICO_IDENTIFIER_H
