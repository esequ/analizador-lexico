#ifndef ANALIZADORLEXICO_PLUS_H
#define ANALIZADORLEXICO_PLUS_H

#include "../AbstractStateMachine.h"

class Plus: public AbstractStateMachine{
public:
    Plus();
    int evaluate() override;
};


#endif