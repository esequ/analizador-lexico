#include "Numeric.h"
#include "../Clasification.h"

Numeric::Numeric() {
    this->state = "q3";
}

int Numeric::evaluate() {
    if(this->state == "q3") {
        if(Clasification::isNumber(this->actualCharacter)) {
            return 3;
        }
        if(Clasification::getAscii(this->actualCharacter) == (int) '.') {
            this->state = "q4";
            return 4;
        }
        return 101;
    }
    if(this->state == "q4") {
        if(Clasification::isNumber(this->actualCharacter)) {
            this->state = "q5";
            return 5;
        }
        return 503;
    }
    if(this->state == "q5") {
        if(Clasification::isDelimiter(this->actualCharacter)) {
            return 102;
        }
        if(Clasification::isNumber(this->actualCharacter)) {
            return 5;
        }
        if(Clasification::getAscii(this->actualCharacter) == (int) 'e'
        || Clasification::getAscii(this->actualCharacter) == (int) 'E') {
            this->state = "q6";
            return 6;
        }
        return 504;
    }
    if(this->state == "q6") {
        if(Clasification::getAscii(this->actualCharacter) == (int) '+'
        || Clasification::getAscii(this->actualCharacter) == (int) '-') {
            this->state = "q7";
            return 7;
        }
        if(Clasification::isNumber(this->actualCharacter)) {
            this->state = "q8";
            return 8;
        }
        return 505;
    }
    if(this->state == "q7") {
        if(Clasification::isNumber(this->actualCharacter)) {
            this->state = "q8";
            return 8;
        }
        return 506;
    }
    if(this->state == "q8") {
        if(Clasification::isDelimiter(this->actualCharacter)) {
            return 103;
        }
        if(Clasification::isNumber(this->actualCharacter)) {
            return 8;
        }
        return 507;
    }

    return 501;
}
