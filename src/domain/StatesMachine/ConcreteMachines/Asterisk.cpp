//
// Created by Jarvis on 19/05/2021.
//

#include "Asterisk.h"
#include "../Clasification.h"

Asterisk::Asterisk() {
    this->state = "q18";
}

int Asterisk::evaluate() {
    if(this->state == "q18") {
        if(Clasification::isAsterisk(this->actualCharacter)) {
            return 106;
        }
        if(Clasification::isEquals(this->actualCharacter)) {
            this->state = "q19";
            return 17;
        }
        return 106;
    }
    if(this->state == "q19") {
        return 107;
    }
}
