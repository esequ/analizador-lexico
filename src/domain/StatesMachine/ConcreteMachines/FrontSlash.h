#ifndef ANALIZADORLEXICO_FRONTSLASH_H
#define ANALIZADORLEXICO_FRONTSLASH_H

#include "../AbstractStateMachine.h"

class FrontSlash: public AbstractStateMachine {
public:
    FrontSlash();
    int evaluate() override;
};


#endif