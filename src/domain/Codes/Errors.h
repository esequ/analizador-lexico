#ifndef ANALIZADORLEXICO_ERRORS_H
#define ANALIZADORLEXICO_ERRORS_H

#include <string>

class Errors {
public:
    int code;
    std::string message;
    Errors(int code, std::string message);
    std::string display() const;

};


#endif //ANALIZADORLEXICO_ERRORS_H
