#include "Errors.h"

#include <utility>

Errors::Errors(int code, std::string message) : code(code), message(std::move(message)) {}

std::string Errors::display() const {
    std::string error = "Error: " + std::to_string(this->code) + " " + this->message;
    return error;
}
