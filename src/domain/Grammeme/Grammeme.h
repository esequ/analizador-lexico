#ifndef GRAMMEME_H
#define GRAMMEME_H

#include <string>
#include "../Lexeme/Lexeme.h"

class Grammeme {
public:
    static Grammeme getGrammeme(Lexeme lexeme);
    const std::string &getValue() const;
private:
    std::string value;
    explicit Grammeme(std::string value);
    static std::string analize(const Lexeme& lexeme);
    static char * splitCharacters(std::string lexeme);
    static bool isReserved(const Lexeme& lexeme);

    static const std::string reservedWords[32];
};


#endif
