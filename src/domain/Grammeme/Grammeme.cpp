#include "Grammeme.h"
#include "../StatesMachine/Clasification.h"
#include <algorithm>
#include "iostream"
#include <utility>
const std::string  Grammeme::reservedWords[] = {"class", "endclass", "int", "float", "char", "string", "bool", "if", "else", "elseif", "endif", "do", "eval", "enddo", "while", "endwhile", "read", "write", "def","as","for", "endfor", "private", "public", "protected", "library", "func", "endfunc", "main", "endmain", "true", "false"};
Grammeme::Grammeme(std::string value) : value(std::move(value)) {}

Grammeme Grammeme::getGrammeme(Lexeme lexeme) {
    std::string value = analize(lexeme);
    return Grammeme(value);
}

std::string Grammeme::analize(const Lexeme& lexeme) {
    if (Grammeme::isReserved(lexeme)) {
        return "Palabra reservada";
    }
    if(lexeme.code == 100) {
        return "Identificador";
    }
    if(lexeme.code == 101) {
        return "Entero";
    }
    if(lexeme.code == 102) {
        return "Decimal";
    }
    if(lexeme.code == 103) {
        return "Notación Cientifica";
    }
    if(lexeme.code == 104) {
        return "Caracter";
    }
    if(lexeme.code == 105) {
        return "Constante String";
    }
    if(lexeme.code == 106) {
        return "Operador aritmético";
    }
    if(lexeme.code == 107) {
        return "Operador de asignación";
    }
    if(lexeme.code == 108) {
        return "Comentario de bloque";
    }
    return "No covered";
}

bool Grammeme::isReserved(const Lexeme& lexeme) {
    int n = sizeof(reservedWords)/sizeof(reservedWords[0]);
    auto isReserved = find(reservedWords, reservedWords+n, lexeme.value);
    if (isReserved != end(reservedWords)) {
        return true;
    }
    return false;
}

char * Grammeme::splitCharacters(std::string lexeme) {
    std::string value = lexeme;
    auto n = value.length();
    char * characters = new char[n+1];
    strcpy_s(characters,n+1, value.c_str());
    return characters;
}

const std::string &Grammeme::getValue() const {
    return value;
}
